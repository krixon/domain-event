<?php

namespace Krixon\DomainEvent\Storage\Exception;

class EventStreamNotFoundException extends EventStoreException
{
    
}
